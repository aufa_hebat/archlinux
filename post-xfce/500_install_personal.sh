#!/bin/bash
set -e

echo "##########    XFCE GLOBAL MENU    ##########"
yay         -S --noconfirm --needed vala-panel-appmenu-xfce-git
yay 		-S --noconfirm --needed vala-panel-appmenu-registrar-git
sudo pacman -S --noconfirm --needed appmenu-gtk-module

# Install windowck
yay 		-S --noconfirm --needed libwnck
yay 		-S --noconfirm --needed xfce4-windowck-plugin

# Install Themes
yay 		-S --noconfirm --needed mojave-gtk-theme-git

# COMPIZ
# Install compiz 0.9
# yay 		-S --noconfirm --needed compiz

# Install compiz 0.8
yay 		-S --noconfirm --needed compiz-core
yay 		-S --noconfirm --needed compiz-gtk
yay 		-S --noconfirm --needed ccsm
yay 		-S --noconfirm --needed compiz-fusion-plugins-main
yay 		-S --noconfirm --needed compiz-fusion-plugins-extra
yay 		-S --noconfirm --needed compiz-fusion-plugins-experimental
yay 		-S --noconfirm --needed fusion-icon

yay 		-S --noconfirm --needed emerald
yay 		-S --noconfirm --needed emerald-themes

# Install xfce4 panel compiz
yay 		-Rns	--noconfirm xfce4-panel
yay			-S 	--noconfirm --needed xfce4-panel-compiz

# change default wm from xfwm4 to compiz
xfconf-query -c xfce4-session -p /sessions/Failsafe/Client0_Command -t string -sa compiz
# xfconf-query -c xfce4-session -p /sessions/Failsafe/Client0_Command -t string -t string -s compiz -s --loose-binding

echo "################################################################"
echo "#########   compiz --replace --loose-binding --indirect-rendering   ################"
echo "################################################################"
# compiz --replace --loose-binding --indirect-rendering

# DOCK
# sudo pacman -S --noconfirm --needed plank

# DockBarX
# yay -S --noconfirm --needed gnome-python-desktop

# DOCK
sudo pacman -S --noconfirm --needed cairo-dock
sudo pacman -S --noconfirm --needed cairo-dock-plug-ins
yay 		-S --noconfirm --needed cairo-dock-themes
yay 		-S --noconfirm --needed cairo-dock-plug-ins-extras

echo "Copy personal to ~"
rm -rf ~/.config
cp -rfT personal ~

echo "################################################################"
echo "#########   Personal have been copied and loaded   ################"
echo "################################################################"