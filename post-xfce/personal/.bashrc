#
# ~/.bashrc
#

source /usr/share/git/completion/git-prompt.sh


# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export HISTCONTROL=ignoreboth:erasedups

# clrreset='\e[0m'
# clrwhite='\e[1;37m'
# clrgreen='\e[1;32m'
# clrred='\e[1;31m'
# clryellow='\e[1;33m'
# clrblue='\e[1;34m'
# clrpurple='\e[1;35m'
# clrcyan='\e[1;36m'
export PS1='\[\e[1;31m\][\[\e[1;32m\]\u@\h \[\e[1;37m\]\W\[\e[1;31m\]]$(__git_ps1 "\[\e[1;35m\](\[\e[1;36m\]git\[\e[1;35m\])-[\[\e[1;34m\]%s\[\e[1;35m\]]")\[\e[1;33m\]\\$ \[\e[0m\]'


if [ -d "$HOME/.bin" ] ;
	then PATH="$HOME/.bin:$PATH"
fi

#list
alias ls='ls --color=auto'
alias la='ls -a'
alias ll='ls -la'
alias l='ls' 					
alias l.="ls -A | egrep '^\.'"    

#fix obvious typo's
alias cd..='cd ..'
alias pdw="pwd"

## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

#readable output
alias df='df -h'

#pacman unlock
alias unlock="sudo rm /var/lib/pacman/db.lck"

#free
alias free="free -mt"

#continue download
alias wget="wget -c"

#userlist
alias userlist="cut -d: -f1 /etc/passwd"

#merge new settings
alias merge="xrdb -merge ~/.Xresources"

# Aliases for software managment
# pacman or pm
alias pacman='sudo pacman --color auto'
alias update='sudo pacman -Syyu'

# yay as aur helper - updates everything
alias pksyua="yay -Syu --noconfirm"

#ps
alias ps="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"

#grub update
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

#improve png
alias fixpng="find . -type f -name "*.png" -exec convert {} -strip {} \;"

#add new fonts
alias fc='sudo fc-cache -fv'

#quickly kill conkies
alias kc='killall conky'

#hardware info --short
alias hw="hwinfo --short"

#skip integrity check
alias yayskip='yay -S --mflags --skipinteg'

#check vulnerabilities microcode
alias microcode='grep . /sys/devices/system/cpu/vulnerabilities/*'

#get fastest mirrors in your neighborhood 
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

#shopt
shopt -s autocd # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s histappend # do not overwrite history
shopt -s expand_aliases # expand aliases

#Recent Installed Packages
alias rip="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -100"

#Cleanup orphaned packages
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'

#create a file called .bashrc-personal and put all your personal aliases
#in there. They will not be overwritten by skel.

[[ -f ~/.bashrc-personal ]] && . ~/.bashrc-personal

neofetch

# Build Android
export USE_CCACHE=1
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk
export ANDROID_JACK_VM_ARGS="-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx15G"
export _JAVA_OPTIONS='-Dsun.java2d.opengl=true'
export ANDROID_SDK=/home/aufa/Android/Sdk
export PATH=${PATH}:$ANDROID_SDK/tools:$ANDROID_SDK/platform-tools

# Android Virtual Device
export PATH=$ANDROID_SDK/emulator:$ANDROID_SDK/tools:$PATH
export zipalign=/home/aufa/Android/Sdk/build-tools/28.0.3

# composer > laravel
export PATH="~/.config/composer/vendor/bin:$PATH"

# visual studio code > trash 
export ELECTRON_TRASH=trash-cli

# Set Proxy
function setproxy() {
    export {http,https,ftp}_proxy="http://muzaidi:muzaidimarzuki27@proxysvr.putrajaya.local:8080"
}

# Unset Proxy
function unsetproxy() {
    unset {http,https,ftp}_proxy
}

# oracle database
export ORACLE_HOME=/usr/lib/oracle/product/11.2.0/xe
export ORACLE_SID=XE
export NLS_LANG=`$ORACLE_HOME/bin/nls_lang.sh`
export PATH=$PATH:$ORACLE_HOME/bin