#!/bin/bash
set -e

# XORG
sudo pacman -S --noconfirm --needed xorg-server xorg-apps xorg-xinit

# installing displaymanager or login manager
sudo pacman -S --noconfirm --needed lightdm
yay 		-S --noconfirm --needed lightdm-slick-greeter
yay 		-S --noconfirm --needed lightdm-settings
sudo pacman -S --noconfirm --needed accountsservice

sudo cp -rf system/etc/lightdm/lightdm.conf /etc/lightdm/
sudo cp -rf system/etc/lightdm/slick-greeter.conf /etc/lightdm/


# enabling displaymanager or login manager
sudo systemctl enable lightdm.service -f
sudo systemctl set-default graphical.target

# installing desktop environment
sudo pacman -S --noconfirm --needed xfce4 xfce4-goodies
yay 		-S --noconfirm --needed mugshot
yay 		-S --noconfirm --needed menulibre
yay 		-S --noconfirm --needed yad
# yay 		-S --noconfirm --needed xfce4-panel-profiles
sudo pacman -S --noconfirm --needed network-manager-applet

# remove unused xfce goodies
yay			-Rns	--noconfirm xfce4-artwork
yay			-Rns	--noconfirm xfburn

# activate lock screen
# 20191002 light locker has bugs. Screen frozen for 2nd lock
# sudo pacman -S --noconfirm --needed light-locker
# yay 		-S --noconfirm --needed light-locker-settings
# xfconf-query -c xfce4-session -p /general/LockCommand -s "light-locker-command --lock" --create -t string

# instead lock screen using dm-tool
xfconf-query -c xfce4-session -p /general/LockCommand -s "dm-tool lock" --create -t string

# manage user directories
sudo pacman -S --noconfirm --needed xdg-user-dirs