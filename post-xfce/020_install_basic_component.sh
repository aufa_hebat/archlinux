#!/bin/bash
set -e

sudo pacman -Syyu --noconfirm

#  install yay aur helper
rm -rf yay
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
cd ..
rm -rf yay

# install archlinux-artwork
yay -S --noconfirm --needed archlinux-artwork
sudo find /usr/share/archlinux/icons -name "*64*" -exec cp {} /usr/share/icons/hicolor/64x64/devices \;
yay -Rns --noconfirm archlinux-artwork

# install archlinux wallpaper
sudo pacman -S --noconfirm --needed archlinux-wallpaper

# Creating private folders we use later
[ -d $HOME"/.icons" ] || mkdir -p $HOME"/.icons"
[ -d $HOME"/.themes" ] || mkdir -p $HOME"/.themes"
[ -d $HOME"/.fonts" ] || mkdir -p $HOME"/.fonts"

# Speed Up boot by mask unused services
sudo systemctl mask lvm2-monitor.service

echo "################################################################"
echo "#########   Basic Component have been installed   ################"
echo "################################################################"
