#!/bin/bash
set -e

echo "Installing spotify"
yay -S --noconfirm --needed spotify
# git clone https://aur.archlinux.org/spotify.git
# cd spotify
# curl -sS https://download.spotify.com/debian/pubkey.gpg | gpg --import
# makepkg -sri

echo "################################################################"
echo "#### Spotify installed  #########"
echo "################################################################"
