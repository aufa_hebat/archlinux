#!/bin/bash
set -e


echo "Installing Xtreme Download Manager"
yay -S --noconfirm --needed xdman

echo "################################################################"
echo "#### Xtreme Download Manager installed  #########"
echo "################################################################"
