#!/bin/bash
set -e

echo "Installing PHP & laravel"
sudo pacman -S --noconfirm --needed php composer
sudo pacman -S --noconfirm --needed php-gd
sudo pacman -S --noconfirm --needed composer
# uncomment extension=gd in /etc/php.ini
# uncomment extension=iconv in /etc/php.ini

composer global require laravel/installer
# bashrc
echo "export PATH=$PATH:$HOME/.config/composer/vendor/bin" >> ~/.bashrc

echo "################################################################"
echo "#### PHP & Laravel installed  #########"
echo "################################################################"
