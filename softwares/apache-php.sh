#!/bin/bash
set -e

echo "Installing APACHE & PHP"
sudo pacman -S --noconfirm --needed php php-apache

# 1.edit /etc/httpd/conf/httpd.conf file
# 2.comment out line #LoadModule mpm_event_module modules/mod_mpm_event.so
sudo sed -i 's/LoadModule mpm_event_module modules\/mod_mpm_event.so/#LoadModule mpm_event_module modules\/mod_mpm_event.so/g' /etc/httpd/conf/httpd.conf

# 3.uncomment line #LoadModule mpm_prefork_module modules/mod_mpm_prefork.so
sudo sed -i 's/#LoadModule mpm_prefork_module modules\/mod_mpm_prefork.so/LoadModule mpm_prefork_module modules\/mod_mpm_prefork.so/g' /etc/httpd/conf/httpd.conf

# 4.Append at bottom /etc/httpd/conf/httpd.conf file
LoadModule php7_module modules/libphp7.so
AddHandler php7-script php
Include conf/extra/php7_module.conf

# 5.save and close

# create a test.php file in the Apache root directory, /srv/http
sudo touch /srv/http/test.php

cat > /srv/http/test.php <<EOF
    <?php
        phpinfo();
    ?>
EOF

sudo systemctl start httpd

# 6. http://ip-address/test.php.

echo "################################################################"
echo "#### APACHE & PHP installed  #########"
echo "################################################################"
