#!/bin/bash
set -e


echo "Installing SmartGit"
yay -S --noconfirm --needed smartgit

echo "################################################################"
echo "#### Smartgit installed  #########"
echo "################################################################"
