#!/bin/bash
set -e


echo "Installing Google Chrome"
yay -S --noconfirm --needed google-chrome

# make chrome default in KDE
# System Settings/Applications/Default Application/Web Browser
# System Settings/Applications/File Association: Filter 'html'>xhtml+xml and html>change order


echo "################################################################"
echo "#### Google Chrome installed  #########"
echo "################################################################"
