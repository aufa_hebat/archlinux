#!/bin/bash
set -e


echo "Installing Prime OS"
Steps to install the prime os on Ubuntu!

1. Download .ISO file on official site(primeos.in)
2. Make some folder on root directory(can use pkexec thunar/sudo thunar or other file manager name, if ubuntu maybe "pkexec nautilus"). In this case, I created a folder named "primeos".
3. Open prime os .iso file(with archive manager software) and copy file (initrd.img, install.img, kernel, ramdisk.img, system.sfs) to primeos folder.
4. Make "data" file, you can use 2 ways below:

a. Make data.img with this command:

To make data.img please type this command on terminal:

sudo dd if=/dev/zero of=/primeos/data.img bs=1M count=32768

Wait for the process to complete, an next type this command:

sudo mkfs.ext4 /primeos/data.img

and wait until the process is done.

b. Or, you can just make some folder with the name is "data".

Reference: 
https://www.fosslicious.com/2018/07/h...
https://www.fosslicious.com/2018/08/h...

5. Create Boot menu. An easy way to make a boot menu option is to use the Grub Customizer.
-  Create a new configuration list
-  Give name Prime OS or whatever name that you want.
-  On Type menu, choose Other, and copy this script below on Boot Sequence:

Boot Sequence on grub customizer:

insmod part_gpt
search --file --no-floppy --set=root /primeos/system.sfs
linux /primeos/kernel root=/dev/ram0 androidboot.selinux=permissive buildvariant=userdebug SRC=/primeos
initrd /primeos/initrd.img

6. Save and reboot, if the Grub menu not show when reboot process, you can press SHIFT button on keyboard when on BIOS boot. And then Choose Prime OS to booting on Prime OS. CMIIW, Enjoy. 

echo "################################################################"
echo "#### Prime OS installed  #########"
echo "################################################################"
