#!/bin/bash
set -e

echo "Installing visual studio code"
yay -S --noconfirm --needed visual-studio-code-bin
sudo pacman -S --noconfirm --needed libdbusmenu-glib # fix global menu 

echo "################################################################"
echo "#### Visual Studio Code installed  #############################"
echo "################################################################"
