#!/bin/bash
set -e

# https://linuxconfig.org/install-lutris-on-manjaro
echo "Installing Lutris" 
sudo pacman -S --noconfirm --needed wine winetricks wine-mono wine_gecko vulkan-icd-loader lib32-vulkan-icd-loader vkd3d lib32-vkd3d gvfs

# https://www.reddit.com/r/linux_gaming/comments/s9tzcq/vanilla_wine_cant_properly_run_any_two_tested/
echo "Installing directx and shader compiler libraries from Microsoft"
winetricks d3dx9 d3dcompiler_42 d3dcompiler_43 d3dcompiler_47

echo "################################################################"
echo "#### Lutris installed  #########"
echo "################################################################"
