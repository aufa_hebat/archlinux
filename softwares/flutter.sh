#!/bin/bash
set -e

echo "Installing Flutter Environment"
yay -S --noconfirm --needed flutter

# create group
sudo groupadd flutterusers

# add user to flutterusers group
sudo gpasswd -a $USER flutterusers

# Change folder's group.
sudo chown -R :flutterusers /opt/flutter

# Change permissions of the folder so the user that was just added to the group will be able to write in it:
sudo chmod -R g+w /opt/flutter

# Re-login your terminal in to the newly created group:
newgrp flutterusers

# if get error "Cannot open file, path ='/opt/flutter/bin/cache/' Permission denied"
# Give write permission to the current user
sudo chown -R $USER /opt/flutter

yay -S --noconfirm --needed android-studio

# bashrc
echo "# Android Studio" >> ~/.bashrc
echo "export ANDROID_SDK=$HOME/Android/Sdk" >> ~/.bashrc
echo "export PATH=${PATH}:$ANDROID_SDK/tools:$ANDROID_SDK/platform-tools" >> ~/.bashrc

# install plugin flutter & dart in android studio

# resolve android-licenses
flutter doctor --android-licenses

echo "################################################################"
echo "#### Flutter installed  #########"
echo "################################################################"
