#!/bin/bash
set -e

# https://rtfm.co.ua/en/arch-linux-steam-installation/
#https://linuxhint.com/install_steam_arch_linux/
echo "Installing Steam" 
sudo pacman -S --noconfirm --needed steam

# install proton
sudo pacman -S --noconfirm --needed fontforge lib32-giflib lib32-libxmu lib32-mpg123 lib32-openal lib32-v4l-utils lib32-libpulse lib32-libxslt lib32-gst-plugins-base-libs lib32-vulkan-icd-loader samba opencl-headers vulkan-headers lib32-sdl2 lib32-lcms2 lib32-gettext lib32-glu lib32-libsm lib32-libpcap libgphoto2
sudo pacman -S --noconfirm --needed wine
yay -S --noconfirm --needed spirv-headers-git
yay -S --noconfirm --needed vkd3d-valve-git
yay -S --noconfirm --needed wine-valve-git
yay -S --noconfirm --needed proton

#install protontricks
yay -S --noconfirm --needed python-vdf
yay -S --noconfirm --needed protontricks

# AOE3 :Game is bugged on Proton 5.0.6, but runs perfectly on Proton 4.11-13 with the protontricks command
# open terminal:protontricks 105450 mfc42 winxp l3codecx

echo "################################################################"
echo "#### Steam installed  #########"
echo "################################################################"
