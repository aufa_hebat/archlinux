#!/bin/bash
set -e

echo "Installing oracle db"
yay -S --noconfirm --needed oracle-xe # https://mikewilliamson.wordpress.com/2017/10/22/setting-up-oracle-xe-on-arch-linux/
yay -S --noconfirm --needed oracle-instantclient-basic # https://download.oracle.com/otn_software/linux/instantclient/195000/instantclient-basic-linux.x64-19.5.0.0.0dbru.zip
yay -S --noconfirm --needed oracle-instantclient-sdk # https://download.oracle.com/otn_software/linux/instantclient/195000/instantclient-sdk-linux.x64-19.5.0.0.0dbru.zip
yay -S --noconfirm --needed gqlplus

# bashrc
echo "oracle database" >> ~/.bashrc
echo "export ORACLE_HOME=/usr/lib/oracle/product/11.2.0/xe" >> ~/.bashrc
echo "export ORACLE_SID=XE" >> ~/.bashrc
echo "export NLS_LANG=$ORACLE_HOME/bin/nls_lang.sh" >> ~/.bashrc
echo  "export PATH=$PATH:$ORACLE_HOME/bin" >> ~/.bashrc

# configure user; https://chartio.com/resources/tutorials/how-to-create-a-user-and-grant-permissions-in-oracle/
# https://www.oracletutorial.com/oracle-administration/oracle-grant-all-privileges-to-a-user/
# use GRANT ALL PRIVILEGE TO <user>; if ANY PRIVILEGE invalid.

echo "################################################################"
echo "#### ORACLE-XE installed  #########"
echo "################################################################"
