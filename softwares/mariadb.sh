#!/bin/bash
set -e

echo "Installing mariadb"
sudo pacman -S --noconfirm --needed mariadb
sudo mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
sudo systemctl start mariadb.service
sudo mysql_secure_installation # Respond with y on all further yes/no prompts, change root password to 'root'
# uncomment extension=mysqli in /etc/php.ini
# uncomment extension=pdo_mysql in /etc/php.ini

echo "################################################################"
echo "#### MariaDB installed  #########"
echo "################################################################"
