#!/bin/bash
set -e

echo "Installing Docker"
sudo pacman -S --noconfirm --needed docker docker-compose

# start service before use
sudo systemctl start docker.service

# create group
sudo groupadd docker

# add user to docker group
# Don’t forget to re-login to apply the changes
sudo gpasswd -a $USER docker

echo "################################################################"
echo "#### Docker installed  #########"
echo "################################################################"
