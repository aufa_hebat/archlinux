#!/bin/bash
set -e
# vmware workstation keys: https://appnee.com/vmware-workstation-pro-universal-license-keys-collection/ 


echo "Installing vmware-workstation"
sudo pacman -S --noconfirm --needed dkms fuse2 gtkmm3 pcsclite
yay -S --noconfirm --needed vmware-keymaps
yay -S --noconfirm --needed vmware-workstation

# run once
sudo modprobe -a vmw_vmci vmmon

# start service before use
sudo systemctl start vmware-networks.service
sudo systemctl start vmware-usbarbitrator.service

# To enable 3D Support
# 1. open ~/.vmware/preferences
# 2. put mks.gl.allowBlacklistedDrivers = "TRUE" at bottom, then Save
echo mks.gl.allowBlacklistedDrivers = "TRUE" >> ~/.vmware/preferences

echo "################################################################"
echo "#### Vmware Workstation installed  #########"
echo "################################################################"
