#!/bin/bash
set -e

echo "Installing WPS Ofiice Suite"
yay -S --noconfirm --needed wps-office
yay -S --noconfirm --needed ttf-wps-fonts

# Microsoft Office file in KDE Plasma is recognized as Zip
sudo rm /usr/share/mime/packages/wps-office-*.xml
sudo update-mime-database /usr/share/mime

echo "################################################################"
echo "#### WPS Office installed  #########"
echo "################################################################"
