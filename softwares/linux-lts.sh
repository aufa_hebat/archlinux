#!/bin/bash
set -e

echo "Installing linux-lts"
sudo pacman -S --noconfirm --needed linux-lts linux-lts-headers
sudo grub-mkconfig -o /boot/grub/grub.cfg
# reboot, then choose Advance option to select lts kernel

echo "################################################################"
echo "#### Linux-LTS installed  #########"
echo "################################################################"
