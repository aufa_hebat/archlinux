#!/bin/bash
set -e

# https://linuxconfig.org/install-lutris-on-manjaro
echo "Installing Microsoft Office" 
sudo pacman -S --noconfirm --needed playonlinux

echo "################################################################"
echo "#### Microsoft Office installed  #########"
echo "################################################################"
