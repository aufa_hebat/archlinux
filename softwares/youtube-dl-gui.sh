#!/bin/bash
set -e


echo "Installing Youtube Downloader GUI"
yay -S --noconfirm --needed python2-twodict-git
yay -S --noconfirm --needed youtube-dl-gui-git

echo "################################################################"
echo "#### Youtube Downloader GUI installed  #########"
echo "################################################################"
