#!/bin/bash
set -e


echo "Installing sublime-text"
yay -S --noconfirm --needed sublime-text-dev
# disable update reminder in user setting preferences "update_check"=false,
echo "To disable update reminder in user setting preferences 'update_check=false'"

echo "################################################################"
echo "#### Software from standard Arch Linux Repo installed  #########"
echo "################################################################"
