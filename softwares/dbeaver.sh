#!/bin/bash
set -e

echo "Installing dbeaver"
sudo pacman -S --noconfirm --needed dbeaver

echo "################################################################"
echo "#### Dbeaver installed  ########################################"
echo "################################################################"
