#!/bin/bash
set -e


echo "Installing category Accessories"

# prevents the screensaver from showing up, or the system from going to sleep
yay -S --noconfirm caffeine-ng

# Flash OS images to SD cards & USB drives
# yay -S --noconfirm etcher-bin

# Flash OS images to SD cards & USB drives
# yay -S --noconfirm mintstick

sudo pacman -S --noconfirm --needed catfish

# calculator
sudo pacman -S --noconfirm --needed galculator


# wallpaper changer
# sudo pacman -S --noconfirm --needed variety

echo "Installing category Development"

yay 		-S --noconfirm --needed sublime-text-dev
# yay 		-S --noconfirm --needed visual-studio-code-bin
# yay 		-S --noconfirm --needed wps-office

# sudo pacman -S --noconfirm --needed meld

echo "Installing category Graphics"

# sudo pacman -S --noconfirm --needed gimp
sudo pacman -S --noconfirm --needed gnome-font-viewer

echo "Installing category Internet"

yay			-S --noconfirm --needed google-chrome

# sudo pacman -S --noconfirm --needed firefox

# yay -S --noconfirm vivaldi
# yay -S --noconfirm vivaldi-codecs-ffmpeg-extra-bin

echo "Installing category Multimedia"

# sudo pacman -S --noconfirm --needed vlc

echo "Installing category System"

# CCleaner
# sudo pacman -S --noconfirm --needed bleachbit

# System Optimizer
# yay -S --noconfirm stacer

# URL retrieval utility
sudo pacman -S --noconfirm --needed curl

# video thumbnail
sudo pacman -S --noconfirm --needed ffmpegthumbnailer

# keyring
sudo pacman -S --noconfirm --needed gnome-keyring

# disk management
# sudo pacman -S --noconfirm --needed gparted

# disk management
# sudo pacman -S --noconfirm --needed gnome-disk-utility

# GTK2 engine to make your desktop look like a 'murrina',
# sudo pacman -S --noconfirm --needed gtk-engine-murrine

# mounting file system
sudo pacman -S --noconfirm --needed gvfs
sudo pacman -S --noconfirm --needed gvfs-mtp
sudo pacman -S --noconfirm --needed gvfs-gphoto2
sudo pacman -S --noconfirm --needed gvfs-afc

# system information
sudo pacman -S --noconfirm --needed hardinfo

# terminal task manager
sudo pacman -S --noconfirm --needed htop

# set numlock on startup
# sudo pacman -S --noconfirm --needed numlockx


# CLI system information tool written in BASH 
yay 		-S --noconfirm --needed neofetch

# mouse pointer theme
yay 		-S --noconfirm --needed oxy-neon
yay 		-S --noconfirm --needed xcursor-breeze

# Manjaro's Package Manager
# yay 		-S --noconfirm --needed pamac-aur
# yay 		-S --noconfirm --needed kalu

# Icon Theme
yay 		-S --noconfirm --needed papirus-icon-theme

# application-level toolkit
sudo pacman -S --noconfirm --needed polkit-gnome
# sudo pacman -S --noconfirm --needed xfce-polkit-git

# Scanner
sudo pacman -S --noconfirm --needed sane
sudo pacman -S --noconfirm --needed simple-scan


# image thumbnail
sudo pacman -S --noconfirm --needed tumbler

# hides your X mouse cursor when you do not need it
# sudo pacman -S --noconfirm --needed unclutter

# Display graphical dialog boxes from shell scripts
sudo pacman -S --noconfirm --needed zenity

# yay -S --noconfirm conky-lua-archers

# CLI system information tool
# yay -S --noconfirm inxi


###############################################################################################

# installation of zippers and unzippers
sudo pacman -S --noconfirm --needed unace unrar zip unzip sharutils  uudeview  arj cabextract file-roller

###############################################################################################

# these come always last to fix harcoded icon
yay -S --noconfirm --needed hardcode-fixer-git
sudo hardcode-fixer

echo "################################################################"
echo "#### Software from standard Arch Linux Repo installed  #########"
echo "################################################################"