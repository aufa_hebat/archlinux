#!/bin/bash

set -e

./000_use_all_cores.sh
./010_install_on_vmware.sh
./020_install_basic_component.sh
./100_install_xfce.sh
./110_install_sound.sh
# ./120_install_bluetooth.sh
# ./130_install_printers.sh
# ./140_install_samba.sh
# ./150_install_network_discovery.sh
./160_install_tlp_for_laptop.sh
./200_install_arch_software.sh
# ./300_install_aur_repo.sh
./400_install_fonts.sh
./500_install_personal.sh

reboot