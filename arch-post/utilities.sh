#!/bin/bash
set -e

echo "Installing NTFS support R/W"
sudo pacman -S --noconfirm --needed ntfs-3g

echo "################################################################"
echo "#### NTFS support installed ####################################"
echo "################################################################"


# Speed Up boot by mask unused services
sudo systemctl mask lvm2-monitor.service
echo "################################################################"
echo "#### Speed Up boot by mask unused services #####################"
echo "################################################################"
