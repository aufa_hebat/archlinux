#!/bin/bash
set -e

. ../computer_settings.sh

if [ "$GRAPHIC_VMWARE" == "true" ]; then

	# to make aufofit guest
	sudo pacman -S --noconfirm --needed xf86-video-vmware
	sudo pacman -S --noconfirm --needed xf86-input-vmmouse

	sudo pacman -S --noconfirm --needed open-vm-tools
	sudo systemctl enable vmtoolsd.service
	sudo systemctl enable vmware-vmblock-fuse.service

	# vsock vmw_vsock_vmci_transport vmw_balloon vmw_vmci vmwgfx
	sudo sed -i '7s/MODULES=()/MODULES=(vsock vmw_vsock_vmci_transport vmw_balloon vmw_vmci vmwgfx)/' /etc/mkinitcpio.conf

	sudo mkinitcpio -p linux
fi

if [ "$GRAPHIC_INTEL" == "true" ]; then
	sudo pacman -S --noconfirm --needed xf86-video-intel vulkan-intel lib32-vulkan-intel
fi

if [ "$GRAPHIC_ATI" == "true" ]; then
	sudo pacman -S --noconfirm --needed xf86-video-ati vulkan-radeon lib32-vulkan-radeon
fi

if [ "$GRAPHIC_NVIDIA" == "true" ]; then
	sudo pacman -S --noconfirm --needed xf86-video-nouveau
fi

# optonal
sudo pacman -S --noconfirm --needed mesa-demos # glxinfo + glxgears

echo "################################################################"
echo "#########   Graphic Driver installed   ################"
echo "################################################################"
