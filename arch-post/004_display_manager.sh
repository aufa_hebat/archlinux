#!/bin/bash
set -e



# XORG
sudo pacman -S --noconfirm --needed xorg

# installing displaymanager or login manager
# sudo pacman -S --noconfirm --needed sddm
yay -S --noconfirm --needed lightdm lightdm-slick-greeter lightdm-settings #lightdm lighter than sddm
sudo sed -i 's/#greeter-session=light-gtk-gnome/greeter-session=lightdm-slick-greeter/g' /etc/lightdm/lightdm.conf

# enabling displaymanager or login manager
# sudo systemctl enable sddm.service -f
sudo systemctl enable lightdm.service -f
# sudo systemctl set-default graphical.target   

echo "################################################################"
echo "#### Display Manager installed ####################################"
echo "################################################################"

