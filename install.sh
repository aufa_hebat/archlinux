#!/bin/bash

. computer_settings.sh

function pause(){
   read -p "$*"
}

IS_UEFI="false" #false=BIOS
if [ -d "/sys/firmware/efi" ]; then
    IS_UEFI="true"
fi

# Update mirror
pacman -S --noconfirm --needed reflector
reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist
# sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist
# reflector --latest 50 --number 20 --sort rate --save /etc/pacman.d/mirrorlist

# Update DB after mirror
pacman -Sy

# UDATE THE SYSTEM CLOCK
timedatectl set-ntp true

# PARTITION THE DISKS

PARTITION_SIZE=1

EFI_PARTITION=1
EFI_SIZE=261

ROOT_PARTITION=1

DATA_PARTITION=1

wipefs -a "$DISK"
# dd if=/dev/zero of="$DISK" bs=512 count=1 conv=notrunc

if [ "$IS_UEFI" == "true" ]; then
	parted -s "$DISK" mklabel msdos
else
	parted -s "$DISK" mklabel gpt
fi

if [ "$IS_UEFI" == "true" ]; then
	EFI_SIZE=$(( PARTITION_SIZE + EFI_SIZE ))

	parted -s -a optimal "$DISK" mkpart primary fat32 "${PARTITION_SIZE}"MiB "${EFI_SIZE}"MiB
	mkfs.fat -F32 "${DISK}$EFI_PARTITION"

	PARTITION_SIZE=$(( PARTITION_SIZE + EFI_SIZE ))
	ROOT_PARTITION=$(( EFI_PARTITION + 1 ))

	if [ "$HAS_PARTITION_DATA" == "true" ]; then
		DATA_PARTITION=$(( ROOT_PARTITION + 1 ))
	fi
fi

# if [ "$HAS_PARTITION_SWAP" == "true" ]
# then
# 	echo "swap partition"
# 	pause 'Press [Enter] key to continue...'

# 	SWAP="$ROOT"
# 	ROOT=$(( $ROOT + $SWAP ))

# 	parted -s -a optimal "$DISK" mkpart primary linux-swap "$PARTITION_SIZE"GB "$SWAP_SIZE"GB
# 	mkswap "$DISK$SWAP"
# 	swapon "$DISK$SWAP"

# 	PARTITION_SIZE=$(( $PARTITION_SIZE + $SWAP_SIZE + 1))
# fi

if [ "$HAS_PARTITION_DATA" == "true" ]; then
	parted -s -a optimal "$DISK" mkpart primary ext4 "${PARTITION_SIZE}"MiB "${ROOT_SIZE}"MiB
	if [ "$IS_UEFI" == "false" ]; then
		parted -s "$DISK" set "$ROOT_PARTITION" boot on
	fi
	mkfs.ext4 "${DISK}$ROOT_PARTITION"

	PARTITION_SIZE=$(( PARTITION_SIZE + ROOT_SIZE ))

	parted -s -a optimal "$DISK" mkpart primary ext4 "$PARTITION_SIZE"MiB 100%
	mkfs.ext4 "${DISK}$DATA_PARTITION"	
else
	parted -s -a optimal "$DISK" mkpart primary ext4 "${PARTITION_SIZE}"MiB 100%
	if [ "$IS_UEFI" == "false" ]; then
		parted -s "$DISK" set "$ROOT_PARTITION" boot on
	fi
	mkfs.ext4 "${DISK}$ROOT_PARTITION"	
fi


mount "${DISK}$ROOT_PARTITION" /mnt
if [ "$IS_UEFI" == "true" ]; then
	mkdir /mnt/boot
	mount "${DISK}$EFI_PARTITION" /mnt/boot
fi
pacstrap /mnt base base-devel linux linux-firmware linux-headers
genfstab -U /mnt >> /mnt/etc/fstab

cp ./chroot.sh /mnt
cp ./chroot_function.sh /mnt

# CHROOT
# To run a command from the chroot, and exit again append the command to the end of the line:
arch-chroot /mnt ./chroot.sh "$DISK" "$HOSTNAME" "$ROOT_PASSWORD" "$TIMEZONE" "$USER_NAME" "$USER_PASSWORD" "$GRUB_TIMEOUT" "$IS_UEFI"

rm /mnt/chroot.sh
rm /mnt/chroot_function.sh

# REBOOTING

umount -a
reboot

# DISK="/dev/sda"
# PARTITION_SWAP="${DISK}1"
# SWAP_SIZE="2.00GB"
# PARTITION_ROOT="${DISK}2"

# # Hostname of the installed machine.
# HOSTNAME="Archlinux"

# # Root password (leave blank to be prompted).
# ROOT_PASSWORD='admin'

# # Main user to create (by default, added to wheel group, and others).
# USER_NAME='aufa'

# # The main user's password (leave blank to be prompted).
# USER_PASSWORD='aufa'

# # System timezone.
# TIMEZONE='Asia/Kuala_Lumpur'

# GRUB_TIMEOUT='1' 


# # UDATE THE SYSTEM CLOCK
# timedatectl set-ntp true

# # PARTITION THE DISKS

# parted -s "$DISK" mklabel msdos
# parted -s -a optimal "$DISK" mkpart primary linux-swap 0.00GB "$SWAP_SIZE"
# parted -s -a optimal "$DISK" mkpart primary ext4 "$SWAP_SIZE" 100%
# parted -s "$DISK" set 2 boot on

# mkfs.ext4 "$PARTITION_ROOT"

# mkswap "$PARTITION_SWAP"
# swapon "$PARTITION_SWAP"

# mount "$PARTITION_ROOT" /mnt
# pacstrap /mnt base base-devel
# genfstab -U /mnt >> /mnt/etc/fstab

# cp ./chroot.sh /mnt
# cp ./chroot_function.sh /mnt

# # CHROOT
# # To run a command from the chroot, and exit again append the command to the end of the line:
# arch-chroot /mnt ./chroot.sh "$DISK" "$HOSTNAME" "$ROOT_PASSWORD" "$TIMEZONE" "$USER_NAME" "$USER_PASSWORD" "$GRUB_TIMEOUT"

# rm /mnt/chroot.sh
# rm /mnt/chroot_function.sh

# # REBOOTING

# umount -a
# reboot
