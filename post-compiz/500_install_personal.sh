#!/bin/bash
set -e

echo "Copy personal to ~"
rm -rf ~/.config
cp -rfT personal ~


echo "################################################################"
echo "#########   Personal have been copied and loaded   ################"
echo "################################################################"