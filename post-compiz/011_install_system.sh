#!/bin/bash
set -e

#  install yay aur helper
rm -rf yay
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
cd ..
rm -rf yay

sudo pacman -S --noconfirm --needed archlinux-wallpaper

echo "install archlinux-artwork"
yay -S --noconfirm --needed archlinux-artwork
sudo find /usr/share/archlinux/icons -name "*64*" -exec cp {} /usr/share/icons/hicolor/64x64/devices \;
yay -Rns --noconfirm archlinux-artwork


echo "################################################################"
echo "#########   Install System   ################"
echo "################################################################"
