#!/bin/bash
set -e

# XORG
sudo pacman -S --noconfirm --needed xorg-server #xorg-apps xorg-xinit xterm

# installing displaymanager or login manager
sudo pacman -S --noconfirm --needed lightdm
yay 		-S --noconfirm --needed lightdm-slick-greeter
yay 		-S --noconfirm --needed lightdm-settings
sudo cp -rf system/etc/lightdm/lightdm.conf /etc/lightdm/

#enabling displaymanager or login manager
sudo systemctl enable lightdm.service -f
sudo systemctl set-default graphical.target


echo "################################################################"
echo "#########   Install System   ################"
echo "################################################################"
