#!/bin/bash
set -e


echo "Installing category Accessories"

yay -S --noconfirm conky-lua-archers

# yay -S --noconfirm python2-pyparted
yay -S --noconfirm mintstick

yay -S --noconfirm caffeine-ng
yay -S --noconfirm etcher-bin
yay -S --noconfirm stacer

echo "Installing category Development"

yay -S --noconfirm sublime-text-dev
# yay -S --noconfirm visual-studio-code-bin
# yay -S --noconfirm wps-office

echo "Installing category Internet"

# yay -S --noconfirm vivaldi
# yay -S --noconfirm vivaldi-codecs-ffmpeg-extra-bin

echo "Installing category System"

# sh AUR/install-downgrade-v*.sh
yay -S --noconfirm inxi
yay -S --noconfirm neofetch
# yay -S --noconfirm numix-circle-icon-theme-git
yay -S --noconfirm oxy-neon
yay -S --noconfirm pamac-aur
yay -S --noconfirm papirus-icon-theme
yay -S --noconfirm ttf-font-awesome
yay -S --noconfirm ttf-font-awesome
yay -S --noconfirm xcursor-breeze

# these come always last

yay -S --noconfirm hardcode-fixer-git
sudo hardcode-fixer

echo "################################################################"
echo "####        Software from AUR Repository installed        ######"
echo "################################################################"