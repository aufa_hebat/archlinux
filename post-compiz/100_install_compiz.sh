#!/bin/bash
set -e

# sudo mkdir /usr/share/xsessions
# sudo nano /usr/share/xsessions/compiz.desktop

yay 		-S --noconfirm --needed  ccsm compiz-fusion-plugins-main compiz-fusion-plugins-extra compiz-fusion-plugins-experimental emerald emerald-themes fusion-icon



#installing desktop environment
sudo pacman -S --noconfirm --needed xfce4-terminal
# yay 		-S --noconfirm --needed sublime-text-dev
# sudo pacman -S --noconfirm --needed dolphin
# sudo pacman -S --noconfirm --needed file-roller
# sudo pacman -S --noconfirm --needed gvfs
# yay 		-S --noconfirm --needed gksu
# sudo pacman -S --noconfirm --needed polkit-gnome
# sudo pacman -S --noconfirm --needed gnome-keyring
# yay 		-S --noconfirm --needed xfce4-panel-compiz
# sudo pacman -S --noconfirm --needed xfce4-notifyd
# sudo pacman -S --noconfirm --needed xfce4-power-manager
# sudo pacman -S --noconfirm --needed xfce4-whiskermenu-plugin
# 
# sudo pacman -S --noconfirm --needed networkmanager
# sudo pacman -S --noconfirm --needed networkmanager-dispatcher-ntpd
# sudo pacman -S --noconfirm --needed network-manager-applet
# 
# sudo pacman -S --noconfirm --needed dconf-editor
# sudo pacman -S --noconfirm --needed xdg-user-dirs
# sudo pacman -S --noconfirm --needed numix-gtk-theme
# sudo pacman -S --noconfirm --needed lxappearance
# sudo pacman -S --noconfirm --needed volumeicon
# sudo pacman -S --noconfirm --needed gmrun
# sudo pacman -S --noconfirm --needed oblogout
# 
# yay 		-S --noconfirm --needed plank
# yay 		-S --noconfirm --needed numix-icon-theme-git # install this first
# yay 		-S --noconfirm --needed numix-circle-icon-theme-git
# yay 		-S --noconfirm --needed menulibre
# yay 		-S --noconfirm --needed xfce4-panel-profiles

# compiz session
# sudo cp -rfT system/usr/share/xsessions /usr/share/xsessions/
# cp -rfT personal ~
