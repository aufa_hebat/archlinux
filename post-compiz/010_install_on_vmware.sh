#!/bin/bash
set -e

# to make aufofit guest
sudo pacman -S --noconfirm --needed xf86-video-vmware
sudo pacman -S --noconfirm --needed xf86-input-vmmouse

sudo pacman -S --noconfirm --needed open-vm-tools
sudo systemctl enable vmtoolsd.service
sudo systemctl enable vmware-vmblock-fuse.service

# vsock vmw_vsock_vmci_transport vmw_balloon vmw_vmci vmwgfx
sudo sed -i '7s/MODULES=()/MODULES=(vsock vmw_vsock_vmci_transport vmw_balloon vmw_vmci vmwgfx)/' /etc/mkinitcpio.conf

sudo mkinitcpio -p linux

echo "################################################################"
echo "#########   Personal have been copied and loaded   ################"
echo "################################################################"