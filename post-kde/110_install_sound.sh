#!/bin/bash
set -e

#Sound
sudo pacman -S pulseaudio --noconfirm --needed
sudo pacman -S pulseaudio-alsa --noconfirm --needed

echo "################################################################"
echo "#########   sound software software installed   ################"
echo "################################################################"