#!/bin/bash
set -e

# Speed Up boot by mask unused services
sudo systemctl mask lvm2-monitor.service

# XORG
sudo pacman -S --noconfirm --needed xorg-server

# installing displaymanager or login manager
# sudo pacman -S --noconfirm --needed sddm
yay -S --noconfirm --needed lightdm lightdm-slick-greeter lightdm-settings #lightdm lighter than sddm
sudo sed -i 's/#greeter-session=light-gtk-gnome/greeter-session=lightdm-slick-greeter/g' /etc/lightdm/lightdm.conf

# enabling displaymanager or login manager
# sudo systemctl enable sddm.service -f
sudo systemctl enable lightdm.service -f
# sudo systemctl set-default graphical.target

# installing minimal KDE desktop environment
sudo pacman -S --noconfirm --needed plasma-desktop
sudo pacman -S --noconfirm --needed kdeplasma-addons
sudo pacman -S --noconfirm --needed breeze-gtk kde-gtk-config
sudo pacman -S --noconfirm --needed kinfocenter
sudo pacman -S --noconfirm --needed kscreen
sudo pacman -S --noconfirm --needed plasma-nm
sudo pacman -S --noconfirm --needed plasma-pa
sudo pacman -S --noconfirm --needed kwallet-pam kwalletmanager
sudo pacman -S --noconfirm --needed powerdevil
sudo pacman -S --noconfirm --needed kgamma5
sudo pacman -S --noconfirm --needed bluedevil

sudo pacman -S --noconfirm --needed packagekit-qt5 #Discover backend for Archlinux repo
sudo pacman -S --noconfirm --needed fwupd #Discover update firmware support


# Install minimal kde-applications
sudo pacman -S --noconfirm --needed konsole #terminal
sudo pacman -S --noconfirm --needed dolphin dolphin-plugins # file manager
sudo pacman -S --noconfirm --needed thunar # Thunar file manager, Dolphin doesn't have open as root
sudo pacman -S --noconfirm --needed kate #text editor
sudo pacman -S --noconfirm --needed ark lrzip lzop p7zip unarchiver unrar # archive
sudo pacman -S --noconfirm --needed gwenview #image viewer
sudo pacman -S --noconfirm --needed kcalc # calculator

sudo pacman -S --noconfirm --needed filelight
sudo pacman -S --noconfirm --needed kcolorchooser
sudo pacman -S --noconfirm --needed kdegraphics-thumbnailers ffmpegthumbs #Thumbnail generation
sudo pacman -S --noconfirm --needed kdf
sudo pacman -S --noconfirm --needed kdialog

sudo pacman -S --noconfirm --needed kget
sudo pacman -S --noconfirm --needed kmix
sudo pacman -S --noconfirm --needed knotes
sudo pacman -S --noconfirm --needed kompare
sudo pacman -S --noconfirm --needed ksystemlog

sudo pacman -S --noconfirm --needed print-manager
sudo pacman -S --noconfirm --needed spectacle
sudo pacman -S --noconfirm --needed sweeper

# Add-on
yay -S --noconfirm --needed checkupdates-aur

echo "Installing Latte Dock"
sudo pacman -S --noconfirm --needed latte-dock
yay -S --noconfirm --needed plasma5-applets-window-buttons
yay -S --noconfirm --needed plasma5-applets-window-title

# add active control + global menu
sudo pacman -S --noconfirm --needed plasma5-applets-active-window-control



# manage user directories
# sudo pacman -S --noconfirm --needed xdg-user-dirs
