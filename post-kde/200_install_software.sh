#!/bin/bash
set -e


echo "Installing category Accessories"

# prevents the screensaver from showing up, or the system from going to sleep
yay -S --noconfirm caffeine-ng

# Flash OS images to SD cards & USB drives
# yay -S --noconfirm etcher-bin

# Flash OS images to SD cards & USB drives
# yay -S --noconfirm mintstick


# wallpaper changer
# sudo pacman -S --noconfirm --needed variety

echo "Installing category Development"

yay 		-S --noconfirm --needed sublime-text-dev
# yay 		-S --noconfirm --needed visual-studio-code-bin
# yay 		-S --noconfirm --needed wps-office

# sudo pacman -S --noconfirm --needed meld

echo "Installing category Graphics"


echo "Installing category Internet"

yay			-S --noconfirm --needed google-chrome

echo "Installing category Multimedia"

# sudo pacman -S --noconfirm --needed vlc

echo "Installing category System"

# GRUB customizer
sudo pacman -S --noconfirm --needed grub-customizer
# CCleaner
# sudo pacman -S --noconfirm --needed bleachbit

# System Optimizer
# yay -S --noconfirm stacer

# URL retrieval utility
sudo pacman -S --noconfirm --needed curl

# video thumbnail
sudo pacman -S --noconfirm --needed ffmpegthumbnailer

# disk management
# sudo pacman -S --noconfirm --needed gparted

# disk management
# sudo pacman -S --noconfirm --needed gnome-disk-utility


# system information
sudo pacman -S --noconfirm --needed hardinfo

# terminal task manager
sudo pacman -S --noconfirm --needed htop


# CLI system information tool written in BASH 
yay 		-S --noconfirm --needed neofetch

# Icon Theme
yay 		-S --noconfirm --needed papirus-icon-theme

# Scanner
sudo pacman -S --noconfirm --needed sane
sudo pacman -S --noconfirm --needed simple-scan



###############################################################################################

# installation of zippers and unzippers
sudo pacman -S --noconfirm --needed unace unrar zip unzip sharutils  uudeview  arj cabextract file-roller

###############################################################################################

# these come always last to fix harcoded icon
yay -S --noconfirm --needed hardcode-fixer-git
sudo hardcode-fixer

echo "################################################################"
echo "#### Software from standard Arch Linux Repo installed  #########"
echo "################################################################"
