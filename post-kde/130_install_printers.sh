#!/bin/bash
set -e


sudo pacman -S --noconfirm --needed cups cups-pdf
sudo pacman -S --noconfirm --needed print-manager #A tool for managing print jobs and printers
sudo pacman -S --noconfirm --needed system-config-printer #auto-detect the printer driver

#first try if you can print without foomatic
#sudo pacman -S foomatic-db-engine --noconfirm --needed
#sudo pacman -S foomatic-db foomatic-db-ppds foomatic-db-nonfree-ppds foomatic-db-gutenprint-ppds --noconfirm --needed
sudo pacman -S ghostscript gsfonts gutenprint --noconfirm --needed
sudo pacman -S gtk3-print-backends --noconfirm --needed
sudo pacman -S libcups --noconfirm --needed
sudo pacman -S hplip --noconfirm --needed


sudo systemctl start org.cups.cupsd.service

echo "After rebooting it will work"

echo "################################################################"
echo "#########   printer management software installed     ##########"
echo "################################################################"
