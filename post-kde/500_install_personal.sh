#!/bin/bash
set -e

echo "remove unused plasma group"
sudo pacman -Rns --noconfirm kwayland-integration
sudo pacman -Rns --noconfirm plasma-sdk
sudo pacman -Rns --noconfirm discover packagekit-qt5 fwupd

echo "Install Kwin Decoration"
yay -S kwin-decoration-sierra-breeze-enhanced-git
# yay -S breeze-blurred-git

echo "Install Akava Color"
# create folder ~/.local/share/color-schemes/
# download from https://github.com/Akava-Design/Akava-Colors
# put Akava.colors, then select from system settings > colors

echo "Install Kvantum"
sudo pacman -S kvantum-qt5

echo "Install Icons"
sudo pacman -S papirus-icon-theme

echo "Install plasma5 applet arch update notifier"
sudo pacman -S qt5-xmlpatterns
yay -S plasma5-applets-kde-arch-update-notifier-git
yay -S checkupdates-aur


sudo pacman -S --noconfirm --needed kvantum-qt5
echo "Copy personal to ~"
rm -rf ~/.config
cp -rfT personal ~

echo "################################################################"
echo "#########   Personal have been copied and loaded   ################"
echo "################################################################"
