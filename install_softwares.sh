#!/bin/bash
set -e


echo "Installing vmware-workstation"
sudo pacman -S --noconfirm --needed dkms fuse2 gtkmm3 pcsclite
yay -S --noconfirm --needed vmware-keymaps
yay -S --noconfirm --needed vmware-workstation

# run once
sudo modprobe -a vmw_vmci vmmon

# start service before use
sudo systemctl start vmware-networks.service

# To enable 3D Support
# 1. open ~/.vmware/preferences
# 2. put mks.gl.allowBlacklistedDrivers = "TRUE" at bottom, then Save
echo mks.gl.allowBlacklistedDrivers = "TRUE" >> ~/.vmware/preferences

echo "Installing visual studio code"
yay -S --noconfirm --needed visual-studio-code-bin
sudo pacman -S --noconfirm --needed libdbusmenu-glib # fix global menu 

echo "Installing sublime-text"
# disable update reminder in user setting preferences "update_check"=false,
yay -S --noconfirm --needed sublime-text-dev

echo "Installing Google Chrome"
yay -S --noconfirm --needed google-chrome

# make chrome default in KDE
# System Settings/Applications/Default Application/Web Browser
# System Settings/Applications/File Association: Filter 'html'>xhtml+xml and html>change order

echo "Installing SmartGit"
yay -S --noconfirm --needed smartgit

echo "Installing Xtreme Download Manager"
yay -S --noconfirm --needed xdman

echo "Installing Docker"
sudo pacman -S --noconfirm --needed docker docker-compose

# start service before use
sudo systemctl start docker.service

# create group
# sudo groupadd docker

# add user to docker group
# Don’t forget to re-login to apply the changes
sudo gpasswd -a $USER docker



echo "Installing Flutter Environment"
yay -S flutter

# create group
sudo groupadd flutterusers

# add user to flutterusers group
sudo gpasswd -a <user> flutterusers

# Change folder's group.
sudo chown -R :flutterusers /opt/flutter

# Change permissions of the folder so the user that was just added to the group will be able to write in it:
sudo chmod -R g+w /opt/flutter

# Re-login your terminal in to the newly created group:
newgrp flutterusers

# if get error "Cannot open file, path ='/opt/flutter/bin/cache/' Permission denied"
# Give write permission to the current user
# sudo chown -R $USER /opt/flutter

yay -S android-studio

# bashrc
# export ANDROID_SDK=/home/aufa/Android/Sdk
# export PATH=${PATH}:$ANDROID_SDK/tools:$ANDROID_SDK/platform-tools

# install plugin flutter & dart in android studio

# resolve android-licenses
# flutter doctor --android-licenses


echo "Installing WPS Ofiice Suite"
yay -S wps-office
yay -S ttf-wps-fonts

# Microsoft Office file in KDE Plasma is recognized as Zip
sudo rm /usr/share/mime/packages/wps-office-*.xml
sudo update-mime-database /usr/share/mime

echo "Installing dbeaver"
sudo pacman -S dbeaver

echo "Installing NTFS support R/W"
sudo pacman -S ntfs-3g

echo "Installing PHP & composer"
sudo pacman -S php composer
sudo pacman -S php-gd
# uncomment extension=gd in /etc/php.ini
# uncomment extension=iconv in /etc/php.ini

echo "Installing Laravel Installer"
composer global require laravel/installer
# add to bashrc - START
 # export PATH="$PATH:$HOME/.config/composer/vendor/bin"
# add to bashrc - END

echo "Installing oracle db"
yay -S oracle-xe # https://mikewilliamson.wordpress.com/2017/10/22/setting-up-oracle-xe-on-arch-linux/
yay -S oracle-instantclient-basic # https://download.oracle.com/otn_software/linux/instantclient/195000/instantclient-basic-linux.x64-19.5.0.0.0dbru.zip
yay -S oracle-instantclient-sdk # https://download.oracle.com/otn_software/linux/instantclient/195000/instantclient-sdk-linux.x64-19.5.0.0.0dbru.zip
yay -S gqlplus

# add to bashrc - START
# oracle database
# export ORACLE_HOME=/usr/lib/oracle/product/11.2.0/xe
# export ORACLE_SID=XE
# export NLS_LANG=`$ORACLE_HOME/bin/nls_lang.sh`
# export PATH=$PATH:$ORACLE_HOME/bin
# add to bashrc - END

# configure user; https://chartio.com/resources/tutorials/how-to-create-a-user-and-grant-permissions-in-oracle/
# https://www.oracletutorial.com/oracle-administration/oracle-grant-all-privileges-to-a-user/
# use GRANT ALL PRIVILEGE TO <user>; if ANY PRIVILEGE invalid.

echo "Installing mariadb"
sudo pacman -S mariadb
sudo mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
sudo systemctl start mariadb.service
sudo mysql_secure_installation # Respond with y on all further yes/no prompts, change root password to 'root'
# uncomment extension=mysqli in /etc/php.ini
# uncomment extension=pdo_mysql in /etc/php.ini

echo "Installing spotify"
git clone https://aur.archlinux.org/spotify.git
cd spotify
curl -sS https://download.spotify.com/debian/pubkey.gpg | gpg --import
makepkg -sri

echo "Installing linux-lts"
sudo pacman -S linux-lts linux-lts-headers
sudo grub-mkconfig -o /boot/grub/grub.cfg
# reboot, then choose Advance option to select lts kernel

echo "Installing Steam" #https://linuxhint.com/install_steam_arch_linux/
sudo pacman -S steam
sudo pacman -S appmenu-gtk-module
sudo pacman -S fontforge lib32-giflib lib32-libxmu lib32-mpg123 lib32-openal lib32-v4l-utils lib32-libpulse lib32-libxslt lib32-gst-plugins-base-libs lib32-vulkan-icd-loader samba opencl-headers vulkan-headers lib32-sdl2 lib32-lcms2 lib32-gettext lib32-glu lib32-libsm lib32-libpcap libgphoto2
sudo pacman -S wine
yay -S spirv-headers-git
yay -S vkd3d-valve-git
yay -S wine-valve-git
yay -S proton

echo "################################################################"
echo "#### Software from standard Arch Linux Repo installed  #########"
echo "################################################################"
