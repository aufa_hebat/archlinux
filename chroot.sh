#!/bin/bash

. chroot_function.sh

DISK="$1"
HOSTNAME="$2"
ROOT_PASSWORD="$3"
TIMEZONE="$4"
USER_NAME="$5"
USER_PASSWORD="$6"
GRUB_TIMEOUT="$7"
IS_UEFI="$8"


echo 'Setting timezone'
set_timezone "$TIMEZONE"

# set the hardware clock from the systemclock
hwclock --systohc

# LOCALE
# sed -i 's/^# *\(en_US.UTF-8\)/\1/' /etc/locale.gen
# sed -ie "176 s/^#//" /etc/locale.gen
sed -i "/#en_US.UTF-8/"'s/^#//' /etc/locale.gen

locale-gen

echo LANG=en_US.UTF-8 > /etc/locale.conf
echo LANGUAGE=en_US >> /etc/locale.conf
echo LC_ALL=C >> /etc/locale.conf
# echo LC_COLLATE=C >> /etc/locale.conf

# HOSTNAME
echo "$HOSTNAME" > /etc/hostname

echo 127.0.0.1 localhost >> /etc/hosts
echo ::1     localhost >> /etc/hosts
echo 127.0.0.1 "$HOSTNAME".localdomain "$HOSTNAME" >> /etc/hosts

# NETWORK CONFIGURATION
pacman -S networkmanager --noconfirm --needed
pacman -S network-manager-applet --noconfirm --needed
systemctl enable NetworkManager

echo 'Configuring sudo'
set_sudoers

# SET ROOT PASSWORD
if [ -z "$ROOT_PASSWORD" ]
then
    echo 'Enter the root password:'
    stty -echo
    read ROOT_PASSWORD
    stty echo
fi
echo 'Setting root password'
set_root_password "$ROOT_PASSWORD"

# Creating initial user
if [ -z "$USER_NAME" ]
then
    echo "Enter the user name"
    stty -echo
    read USER_NAME
    stty echo
fi

if [ -z "$USER_PASSWORD" ]
then
    echo "Enter the password for user $USER_NAME"
    stty -echo
    read USER_PASSWORD
    stty echo
fi
echo 'Creating initial user'
create_user "$USER_NAME" "$USER_PASSWORD"

# Install Bootloader
pacman -S grub --noconfirm --needed

if [ "$IS_UEFI" == "true" ] 
then
	pacman -S efibootmgr --noconfirm --needed
	grub-install --target=x86_64-efi --efi-directory=/boot
else
	grub-install "$DISK"
fi

# This makes the grub timeout 2, it's faster than 5 :)
sed -i "s/GRUB_TIMEOUT=5/GRUB_TIMEOUT=""$GRUB_TIMEOUT""/g" /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg

# # Building a swapfile
# # Choose the size of your swapfile
# # Here I chose 2GB
# fallocate -l 2G /swapfile

# # Set the permissions on the swapfile
# chmod 600 /swapfile

# # make the directory swapfile a ...swapfile
# mkswap /swapfile

# # Edit /etc/fstab with the swapfile configuration
# echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab

# # Verify /etc/fstab has been updated appropriately
# cat /etc/fstab

# # You should see:
# 	#/
# 	#/home
# 	#/swapfile none swap sw 0 0


# MULTILIB REPOSITORY include applications such as wine and steam
sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf
# Enable Color & TotalDownload
sed -i "/Color/,/TotalDownload/"'s/^#//' /etc/pacman.conf
# change default progress bar
sed -i "/VerbosePkgLists/ a ILoveCandy" /etc/pacman.conf
pacman -Syu

# BASH COMPLETION
pacman -S bash-completion --noconfirm --needed

# git
pacman -S git --noconfirm --needed

# wget
pacman -S wget --noconfirm --needed

# reflector
pacman -S reflector --noconfirm --needed
