#!/bin/bash

DISK="/dev/sda"

HAS_PARTITION_SWAP="false"
SWAP_SIZE="2" #MiB

HAS_PARTITION_DATA="true"

ROOT_SIZE=25600 #MiB

# Hostname of the installed machine.
HOSTNAME="Archlinux"

# Root password (leave blank to be prompted).
ROOT_PASSWORD='admin'

# Main user to create (by default, added to wheel group, and others).
USER_NAME='aufa'

# The main user's password (leave blank to be prompted).
USER_PASSWORD='aufa'

# System timezone.
TIMEZONE='Asia/Kuala_Lumpur'

# GRUB timeout.
# default 5 sec
GRUB_TIMEOUT=1

#hybrid graphic
HYBRID_GRAPHIC="false"

#intel graphic
GRAPHIC_INTEL="false"
#ati graphic
GRAPHIC_ATI="false"
#nvidia graphic
GRAPHIC_NVIDIA="false"
#vmware graphic
GRAPHIC_VMWARE="true"
